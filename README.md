Deconvolution practicals
=========================
Implementation of various deconvolution approaches in Matlab used to teaching.

## Content
- exercices : functions with blanks
- solutions : the solution to the exercices
- data : datasets 
- utils : various useful functions

Author: Jerome Boulanger
